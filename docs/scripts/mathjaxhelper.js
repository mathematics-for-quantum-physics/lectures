MathJax.Hub.Config({
  "fast-preview": {disabled:true},
  tex2jax: {
    inlineMath: [ ['$','$'] ],
    processEscapes: true
  },
});

