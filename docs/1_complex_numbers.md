---
title: Complex Numbers
---

# 1. Complex Numbers

The lecture on complex numbers consists of three parts, each with their own video:

- [1.1. Definition and basic operations](#11-definition-and-basic-operations)
- [1.2. Complex functions](#12-complex-functions)
- [1.3. Differentiation and integration](#13-differentiation-and-integration)

**Total video length: 38 minutes and 53 seconds**

## 1.1 Definition and basic operations

<iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/fLMdaMuEp8s?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Complex numbers 
!!! info "Definition I"
    Complex numbers are numbers of the form $$z = a + b {\rm i}.$$ 
    Here $\rm i$ is the square root of -1: $${\rm i} = \sqrt{-1},$$ 
    or equivalently: $${\rm i}^2 = -1.$$

Usual operations on numbers have their natural extension for complex
numbers, as we shall see below.

Some useful definitions:

!!! info "Definition II" 
    For a complex number $z = a + b {{\rm i}}$, $a$ is called the *real part*, and $b$ the *imaginary part*.

!!! info "Complex conjugate"
    The *complex conjugate* $z^*$ of $z = a + b {{\rm i}}$ is defined as
    $$z^* = a - b{{\rm i}},$$ 

i.e., taking the complex conjugate means flipping the sign of the imaginary part.

### Addition

!!! info "Addition"
    For two complex numbers, $z_1 = a_1 + b_1 {{\rm i}}$ and $z_2 = a_2 + b_2 {{\rm i}}$, 
    the sum $w = z_1 + z_2$ is given as 
    $$w = w_1 + w_2 {{\rm i}}= (a_1 + a_2) + (b_1 + b_2) {{\rm i}}$$ 

where the parentheses in the rightmost expression have been added to group the real and the imaginary part. A consequence of this definition is that the sum of a complex number and its complex conjugate is real:
$$z + z^* = a + b {{\rm i}}+ a - b {{\rm i}}= 2a,$$ i.e., this results in twice the real part of $z$. 

Similarly, subtracting $z^*$ from $z$ yields $$z - z^* = a + b {{\rm i}} - a + b {{\rm i}}= 2b{\rm i},$$ i.e., twice the imaginary part of $z$ (times $\rm i$).

### Multiplication

!!! info "Multiplication"
    For the same two complex numbers $z_1$ and $z_2$ as above, their product is calculated as
    $$w = z_1 z_2 = (a_1 + b_1 {{\rm i}}) (a_2 + b_2 {{\rm i}}) = (a_1 a_2 - b_1 b_2) + (a_1 b_2 + a_2 b_1) {{\rm i}},$$
    where the parentheses have again beèn used to indicate the real and imaginary parts.

A consequence of this definition is that the product of a complex number
$z = a + b {{\rm i}}$ with its conjugate is real:
$$z z^* = (a+b{{\rm i}})(a-b{{\rm i}}) = a^2 + b^2.$$ 
The square root of this number is called the *norm* $|z|$ of $z$:
$$|z| = \sqrt{z z^*} = \sqrt{a^2 + b^2}.$$

### Division

The quotient $z_1/z_2$ of two complex numbers $z_1$ and $z_2$ defined above can be evaluated by multiplying the numerator and denominator by the complex conjugate of $z_2$:
!!! info "Division"
    $$\frac{z_1}{z_2} = \frac{z_1 z_2^*}{z_2 z_2^*} = \frac{(a_1 a_2 + b_1 b_2) + (-a_1 b_2 + a_2 b_1) {{\rm i}}}{a_2^2 + b_2^2}.$$

Try this yourself!

!!! check "Example:"
    $$\begin{align} 
    \frac{1 + 2{\rm i}}{1 - 2{\rm i}} &= \frac{(1 + 2{\rm i})(1 + 2{\rm i})}{1^2 + 2^2} = \frac{1+4{\rm i} -4}{5}\\
    & = -\frac{3}{5} + {\rm i} \frac{4}{5}
    \end{align}$$

### Visualization: the complex plane

Complex numbers can be rendered on a two-dimensional (2D) plane, the
*complex plane*. This plane is spanned by two unit vectors, one
horizontal representing the real number 1 and the vertical
unit vector representing ${\rm i}$.

<figure markdown>
  ![image](figures/complex_numbers_5_0.svg)
  <figcaption>The norm of $z$ is the length of its vector spanned in the complex plane.</figcaption>
</figure>

#### Addition in the complex plane

Adding two numbers in the complex plane corresponds to adding their
respective horizontal and vertical components:

<figure markdown>
  ![image](figures/complex_numbers_8_0.svg)
  <figcaption>The sum of two complex numbers is found as the diagonal of a parallelogram spanned by the vectors of those two numbers.</figcaption>
</figure>

## 1.2. Complex functions

<iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/7XtR_wDSqRc?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Real functions can (most of the times) be written in terms of a Taylor series expanded at a point $x_{0}$:
$$f(x) = \sum \limits_{n=0}^{\infty} \frac{f^{(n)}(x_{0})}{n!} (x-x_{0})^{n}$$
We can write something similar for complex functions by replacing the *real* variable $x$ with its *complex* counterpart $z$:
$$f(z) = \sum \limits_{n=0}^{\infty} \frac{f^{(n)}(x_{0})}{n!} (z-x_{0})^{n}$$

For this course, the most important function is the *complex exponential function*, at which we will have a closer look below.

### The complex exponential function
The complex exponential is used *extremely often*. 
It occurs in Fourier transforms and it is very convenient for doing calculations involving cosines and sines. 
It also makes many common operations on complex number a lot easier to perform.

!!! info "The exponential function and Euler identity"
    The exponential function $f(z) = \exp(z) = e^z$ is defined as:
    $$\exp(z) = e^{x + {\rm i}y} = e^{x} e^{{\rm i} y} = e^{x} \left( \cos y + {\rm i} \sin y\right).$$
The last expression is called the *Euler identity*.

!!! note "**Exercise**" 
    Check that this function obeys
    $$\exp(z_1) \exp(z_2) = \exp(z_1 + z_2).$$ 
    *You will need sum and difference formulas of cosine and sine.*

### The polar form

A complex number $z$ can be represented by two real numbers, $a$ and $b$, which correspond to the real and imaginary part of the complex number.
Another representation of $z$ is a *vector* in the complex plane with a horizontal component that corresponds to the real part of $z$ and a vertical component that corresponds to the imaginary part of $z$. 
It is also possible to characterize that vector by its *length* and *direction*, where the latter can be represented by the
angle that the vector makes with the horizontal axis:

<figure markdown>
  ![image](figures/complex_numbers_10_0.svg)
  <figcaption>The angle with the horizontal axis is denoted by $\varphi$ 
  like in the case of conventional polar coordinates, 
  but in the context of complex numbers, this angle is called as the <b>argument</b>.</figcaption>
</figure>

!!! info "Polar form of complex numbers"
    A complex number can be represented either by its real and imaginary part 
    corresponding to the Cartesian coordinates in the complex plane,
    or by its *norm* and its *argument* corresponding to polar coordinates. 
    The norm is the length of the vector, and the argument is the angle it makes with the horizontal axis.

We can conclude that for a complex number $z = a + b {\rm i}$, its real and imaginary parts
can be expressed  in polar coordinates as $$a = |z| \cos\varphi$$ $$b = |z| \sin\varphi$$

!!! info "Inverse equations"
    The inverse equations are $$|z| = \sqrt{a^2 + b^2}$$
    $$\varphi = \arctan(b/a)$$ for $a>0$. 
    In general:
    $$\varphi = \begin{cases} \arctan(b/a) &{\rm for ~} a>0; \\
    \pi + \arctan(b/a) & {\rm for ~} a<0 {\rm ~ and ~} b>0;\\
    -\pi + \arctan(b/a) &{\rm for ~} a<0 {\rm ~ and ~} b<0. \end{cases}$$ 
 
It turns out that by using the magnitude $|z|$ and phase $\varphi$, we can write any complex number as
$$z = |z| e^{{\rm i} \varphi}$$
By increasing $\varphi$ by $2 \pi$, we make a full circle around the origin and reach the same point on the complex plane. In other words, by adding $2 \pi$ to the argument of $z$, we get the same complex number $z$!
As a result, the argument $\varphi$ is defined up to $2 \pi$, and we are free to make any choice we like, such as in the examples in the figure below:

<figure markdown>
  ![image](figures/complex_numbers_11_0.svg)
  <figcaption> $-\pi < \varphi < \pi$ (left) and (right) $-\frac{\pi}{2} < \varphi < \frac{3 \pi}{2}$ </figcaption>
</figure>

Some useful values of the complex exponential to know by heart are:

!!! tip "Useful identities:"
    $$e^{2{\rm i } \pi} = 1$$
    $$e^{{\rm i} \pi} = -1 $$ 
    $$e^{{\rm i} \pi/2} = {\rm i}$$
    From the first expression, it also follows that 
    $$e^{{\rm i} (y + 2\pi n)} = e^{{\rm i}y} {\rm ~ for ~} n \in \mathbb{Z}$$
    As a result, $y$ is only defined up to $2\pi$.  

Furthermore, we can define the sine and cosine in terms of complex exponentials:

!!! info "Complex sine and cosine"
    $$\cos(x) = \frac{e^{{\rm i} x} + e^{-{\rm i} x}}{2}$$
    $$\sin(x) = \frac{e^{{\rm i} x} - e^{-{\rm i} x}}{2i}$$

Most operations on complex numbers become easier when complex numbers are converted to their *polar form* using the complex exponential.
Some functions and operations, which are common in real analysis, can be easily derived for their complex counterparts by substituting the real variable $x$ with the complex variable $z$ in its polar form:
!!! info "Examples of some complex functions stated using polar form"
    $$z^{n} = \left(r e^{{\rm i} \varphi}\right)^{n} = r^{n} e^{{\rm i} n \varphi}$$
    $$\sqrt[n]{z} = \sqrt[n]{r e^{{\rm i} \varphi} } = \sqrt[n]{r} e^{{\rm i}\varphi/n} $$
    $$\log(z) = log \left(r e^{{\rm i} \varphi}\right) = log(r) + {\rm i} \varphi$$
    $$z_{1}z_{2} = r_{1} e^{{\rm i} \varphi_{1}} r_{2} e^{{\rm i} \varphi_{2}} = r_{1} r_{2} e^{{\rm i} (\varphi_{1} + \varphi_{2})}$$

Use of polar form lets us notice immediately that for example, as a result of multiplication, the norm of the new number is the *product* of the norms of the multiplied numbers and its argument is the *sum* of the arguments of the multiplied numbers. 

In the complex plane, this looks as follows:

<figure markdown>
  ![image](figures/complex_numbers_12_0.svg)
  <figcaption></figcaption>
</figure>

!!! check "Example: Find all solutions solving $z^4 = 1$."
    Of course, we know that $z = \pm 1$ are two solutions, but which other solutions are possible? We take a systematic approach:
    $$\begin{align} z = e^{{\rm i} \varphi} & \Rightarrow z^4 = e^{4{\rm i} \varphi} = 1 \\
    & \Leftrightarrow 4 \varphi = n 2 \pi \\
    & \Leftrightarrow \varphi = 0, \varphi = \frac{\pi}{2}, \varphi = -\frac{\pi}{2}, \varphi = \pi \\
    & \Leftrightarrow z = 1, z = i, z = -i, z = -1 \end{align}$$

## 1.3. Differentiation and integration

<iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/JyftSqmmVdU?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**We only consider differentiation and integration over *real* variables.** 

We can then regard the complex ${\rm i}$ as another constant, and use our usual differentiation and integration rules:
!!! info "Differentiation and Integration rules"
    $$\frac{d}{d\varphi} e^{{\rm i} \varphi} = e^{{\rm i} \varphi} \frac{d}{d\varphi} ({\rm i} \varphi) ={\rm i} e^{{\rm i} \varphi} .$$
    $$\int_{0}^{\pi} e^{{\rm i} \varphi} = \frac{1}{{\rm i}} \left[ e^{{\rm i} \varphi} \right]_{0}^{\pi} = -{\rm i}(-1 -1) = 2 {\rm i}$$

## 1.4. Bonus: the complex exponential function and trigonometry

Let us show some tricks in the following examples where the simple properties of the exponential
function help in re-deriving trigonometric identities.

!!! example "Properties of the complex exponential function I"
    Take $|z_1| = |z_2| = 1$, and $\arg{(z_1)} = \varphi_1$ and
    $\arg{(z_2)} = \varphi_2$. 
    It is easy to see then that $z_i = \exp({\rm i} \varphi_i)$, $i=1, 2$. Then:
    $$z_1 z_2 = \exp[{\rm i} (\varphi_1 + \varphi_2)].$$ 
    The left hand side can be written as
    $$\begin{align}
    z_1 z_2 & = \left[ \cos(\varphi_1) + {\rm i} \sin(\varphi_1) \right] \left[ \cos(\varphi_2) + {\rm i} \sin(\varphi_2) \right] \\
    & = \cos\varphi_1 \cos\varphi_2 - \sin\varphi_1 \sin\varphi_2 + {\rm i} \left( \cos\varphi_1 \sin\varphi_2 + 
    \sin\varphi_1 \cos\varphi_2 \right).
    \end{align}$$

    Also, the right hand side can be written as
    $$\exp[{\rm i} (\varphi_1 + \varphi_2)] = \cos(\varphi_1 + \varphi_2) + {\rm i} \sin(\varphi_1 + \varphi_2).$$
    Comparing the two expressions, equating their real and imaginary parts, we find
    $$\cos(\varphi_1 + \varphi_2) = \cos\varphi_1 \cos\varphi_2 - \sin\varphi_1 \sin\varphi_2;$$
    $$\sin(\varphi_1 + \varphi_2) = \cos\varphi_1 \sin\varphi_2 + 
    \sin\varphi_1 \cos\varphi_2.$$ 
    Note that we used the Euler formula in order to derive the identities of trigonometric function.
    The point is to show you that you can use the properties of the complex exponential to quickly find the form of trigonometric formulas, which are often easily forgotten.

!!! example "Properties of the complex exponential function II" 
    In this example, let's see what we can learn from the derivative of the exponential function:
    $$\frac{d}{d\varphi} \exp({\rm i} \varphi) = {\rm i} \exp({\rm i} \varphi) .$$
    Writing out the exponential in terms of cosine and sine, we see that
    $$\cos'\varphi + {\rm i} \sin'\varphi = {\rm i} \cos\varphi - \sin\varphi.$$
    where the prime $'$ denotes the derivative as usual. Equating real and imaginary parts leads to 
    $$\cos'\varphi = - \sin\varphi;$$
    $$\sin'\varphi = \cos\varphi.$$

## 1.5. Summary

1.  A complex number $z$ has the form $$z = a + b \rm i$$ where $a$ and
    $b$ are both real, and $\rm i^2 = 1$. The real number $a$ is called
    the *real part* of $z$ and $b$ is the *imaginary part*. Two complex
    numbers can be added, subtracted and multiplied straightforwardly.
    The quotient of two complex numbers $z_1=a_1 + \rm i b_1$ and
    $z_2=a_2 + \rm i b_2$ is
    $$\frac{z_1}{z_2} = \frac{z_1 z_2^*}{z_2 z_2^*} = \frac{(a_1 a_2 + b_1 b_2) + (-a_1 b_2 + a_2 b_1) {{\rm i}}}{a_2^2 + b_2^2}.$$

2.  Complex numbers can also be characterised by their *norm*
    $|z|=\sqrt{a^2+b^2}$ and *argument* $\varphi$. These parameters
    correspond to polar coordinates in the complex plane. For a complex
    number $z = a + b {\rm i}$, its real and imaginary parts can be
    expressed as $$a = |z| \cos\varphi$$ $$b = |z| \sin\varphi$$ The
    inverse equations are $$|z| = \sqrt{a^2 + b^2}$$
    $$\varphi = \begin{cases} \arctan(b/a) &{\rm for ~} a>0; \\
     \pi + \arctan(b/a) & {\rm for ~} a<0 {\rm ~ and ~} b>0;\\
     -\pi + \arctan(b/a) &{\rm ~ for ~} a<0 {\rm ~ and ~} b<0.
     \end{cases}$$
    The complex number itself then becomes
    $$z = |z| e^{{\rm i} \varphi}$$

3.  The most important complex function for us is the complex exponential function, which simplifies many operations on complex numbers
    $$\exp(z) = e^{x + {\rm i}y} = e^{x} \left( \cos y + {\rm i} \sin y\right).$$
    where $y$ is defined up to $2 \pi$.\\
    The $\sin$ and $\cos$ can be rewritten in terms of this complex exponential as
    $$\cos(x) = \frac{e^{{\rm i} x} + e^{-{\rm i} x}}{2}$$
    $$\sin(x) = \frac{e^{{\rm i} x} - e^{-{\rm i} x}}{2i}$$
    Because we only consider *differentiation* and *integration* over *real variables*, the usual rules apply:
    $$\frac{d}{d\varphi} e^{{\rm i} \varphi} = e^{{\rm i} \varphi} \frac{d}{d\varphi} ({\rm i} \varphi) ={\rm i} e^{{\rm i} \varphi} .$$
    $$\int_{0}^{\pi} e^{{\rm i} \varphi} = \frac{1}{{\rm i}} \left[ e^{{\rm i} \varphi} \right]_{0}^{\pi} = -{\rm i}(-1 -1) = 2 {\rm i}$$

## 1.6. Problems

1.  [:grinning:] Given $a=1+2\rm i$ and $b=-3+4\rm i$, calculate and draw in the complex plane the numbers:
    1.  $a+b$,
    2.  $ab$, 
    3.  $b/a$.

2.  [:grinning:] Evaluate:  
    1. $\rm i^{1/4}$, 
    2. $\left(1+\rm i \sqrt{3}\right)^{1/2}$,
    3. $\exp(2\rm i^3)$.

3.  [:grinning:] Find the three 3rd roots of $1$ and ${\rm i}$. </br>
    (i.e. all possible solutions to the equations $x^3 = 1$ and $x^3 = {\rm i}$, respectively).

4.  [:grinning:] *Quotients*</br>
    1. Find the real and imaginary part of $$ \frac{1+ {\rm i}}{2+3{\rm i}} \, .$$
    2. Evaluate for real $a$ and $b$:$$\left| \frac{a+b\rm i}{a-b\rm i} \right| \, .$$ 

5.  [:sweat:] For any given complex number $z$, we can take the inverse $\frac{1}{z}$. 
    1.  Visualize taking the inverse in the complex plane. 
    2.  What geometric operation does taking the inverse correspond to? </br>
    (Hint: first consider what geometric operation $\frac{1}{z^*}$ corresponds to.)

6.  [:grinning:]  *Differentation and integration* </br>
    1. Compute $$\frac{d}{dt} e^{{\rm i} (kx-\omega t)},$$
    2. Calculate the real part of $$\int_0^\infty e^{-\gamma t  +\rm i \omega t} dt$$ 
    ($k$, $x$, $\omega$, $t$ and $\gamma$ are real; $\gamma$ is positive).

7.  [:smirk:] Compute by making use of the Euler identity.
    $$\int_{0}^{\pi}\cos(x)\sin(2x)dx$$
    
