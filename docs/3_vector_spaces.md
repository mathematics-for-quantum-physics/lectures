---
title: Vector Spaces
---

# 3. Vector spaces

The lecture on vector spaces consists of **three parts**:

- [3.1. Definition and basis dependence](#31-definition-and-basis-dependence)

- [3.2. Properties of a vector space](#32-properties-vector-space)

- [3.3. Matrix representation of vectors](#33-matrix-representation-vectors)

and at the end of this lecture note, there is a set of corresponding exercises

- [3.4 Problems](#34-problems)

---

The contents of this lecture are summarised in the following **videos**:

1. [Vector spaces: Introduction](https://www.dropbox.com/s/evytrbb55fgrcze/linear_algebra_01.mov?dl=0)

2. [Operations in vector spaces](https://www.dropbox.com/s/1530xb7zbuhwu6u/linear_algebra_02.mov?dl=0)

3. [Properties of vector spaces](https://www.dropbox.com/s/5lwkxd8lw5uwri9/linear_algebra_03.mov?dl=0)

**Total video lentgh: ~16 minutes**

## 3.1. Definition and basis dependence

A vector $\vec{v}$ is a mathematical object characterised by both a **magnitude** and a **direction**, that is, an orientation in a given space.
  
We can express a vector in terms of its individual **components**. Let's assume we have an $n$-dimensional space, meaning that the vector $\vec{v}$ can be oriented in different ways along each of $n$ dimensions. The expression of $\vec{v}$ in terms of its components is

$$\vec{v} = (v_1, v_2,\ldots, v_n) \, ,$$
 
We will denote by ${\mathcal V}^n$ the **vector space** composed by all possible vectors of the above form.

The components of a vector, $\{ v_i\}$ can be **real numbers** or **complex numbers**,
depending on whether we have a real or a complex vector space. 

!!! info "Vector basis" 
    Note that the above expression of $\vec{v}$ in terms of its components assume that we are using a specific **basis**. 
    It is important   to  recall that the same vector can be expressed in terms of different bases. 
    A **vector basis** is a set of $n$ vectors that can be used to generate all the elements of a vector space.

For example, a possible basis of  ${\mathcal V}^n$ could be denoted by $\vec{a}_1,\vec{a}_2,\ldots,\vec{a_n}$,
and we can write a generic vector  $\vec{v}$  as

$$\vec{v} = (v_1, v_2, \ldots, v_n) = v_1 \vec{a}_1 + v_2 \vec{a}_2 + \ldots v_n \vec{a}_n \, .$$

However, one could choose a different basis, denoted by $\vec{b}_1,\vec{b}_2,\ldots,\vec{b_n}$, where the same vector would be expressed in terms of a different set of components

$$ \vec{v} = (v'_1, v'_2, \ldots, v'_n) = v'_1 \vec{b}_1 + v'_2 \vec{b}_2 + \ldots v'_n \vec{b}_n \, .$$

Thus, while the vector remains the same, the values of its components depend on the specific choice of basis.

The most common basis is the **Cartesian basis**, where for example for $n=3$:

$$\vec{a}_1 = (1, 0, 0) \, ,\qquad \vec{a}_2 = (0, 1, 0)\, ,\qquad \vec{a}_3 = (0, 0, 1) \, .$$
  
!!! warning ""
    The elements of a vector basis must be **linearly independent** from one another, meaning
    that none of them can be expressed as a linear combination of the other basis vectors.

We can consider one example in the two-dimensional real vector space $\mathbb{R}$, namely the $(x,y)$ coordinate plane, shown below.

<figure markdown>
  ![image](figures/3_vector_spaces_1.jpg)
  <figcaption></figcaption>
</figure>
  
In this figure, you can see how the same vector $\vec{v}$ can be expressed in two different bases. In the first one (left panel), the Cartesian basis is used and its components are $\vec{v}=(2,2)$. In the second basis (right panel), the components are different, namely $\vec{v}=(2.4 ,0.8)$, while the magnitude and direction of the vector remain unchanged.

For many problems, both in mathematics and in physics, the appropriate choice of the vector space basis may significantly simplify the
solution process.
    
## 3.2. Properties of a vector space

You might be already familiar with the concept of performing a number of various **operations** between vectors, so in this course, let us review some essential operations that are relevant to start working with quantum mechanics:

!!! info "Addition" 
    I can add two vectors to produce a third vector, $$\vec{a} + \vec{b}= \vec{c}.$$
    As with scalar addition, also vectors satisfy the commutative property, $$\vec{a} + \vec{b} = \vec{b} + \vec{a}.$$
    Vector addition can be carried out in terms of their components,
    $$ \vec{c} = \vec{a} + \vec{b} = (a_1 + b_1, a_2 + b_2, \ldots, a_n + b_n) =  (c_1, c_2, \ldots, c_n).$$

!!! info "Scalar multiplication" 
    I can multiply a vector by a scalar number (either real or complex) to produce another vector, $$\vec{c} = \lambda \vec{a}.$$ 
    Addition and scalar multiplication of vectors are both *associative* and *distributive*, so the following relations hold
    $$\begin{align} &1. \qquad (\lambda \mu) \vec{a} = \lambda (\mu \vec{a}) = \mu (\lambda \vec{a})\\
    &2. \qquad \lambda (\vec{a} + \vec{b}) = \lambda \vec{a} + \lambda \vec{b}\\
    &3. \qquad (\lambda + \mu)\vec{a} = \lambda \vec{a} +\mu \vec{a} \end{align}$$
 
### Vector products

In addition to multiplying a vector by a scalar, as mentioned above, one can also multiply two vectors among them. 
There are two types of vector products; where the end result is a scalar (so just a number) and where the end result is another vector. 

!!! info "Scalar product of vectors" 
    The scalar product of vectors is given by $$ \vec{a}\cdot \vec{b} = a_1b_1 + a_2b_2 + \ldots + a_nb_n \, .$$
    Note that since the scalar product is just a number, its value will not depend on the specific
    basis in which we express the vectors: the scalar product is said to be *basis-independent*. The scalar product is also found via 
    $$\vec{a} \cdot \vec{b} = |\vec{a}||\vec{b}| \cos \theta$$ with $\theta$ the angle between the vectors.

!!! info "Cross product"
    The vector product (or cross product) between two vectors $\vec{a}$ and $\vec{b}$ is given by 
    $$ \vec{a}\times \vec{b} = |\vec{a}||\vec{b}|\sin\theta \hat{n}$$
    where $|\vec{a}|=\sqrt{ \vec{a}\cdot\vec{a} }$ (and likewise for $|\vec{b}|$) is the norm of the vector $\vec{a}$, $\theta$ is the angle between the two vectors, and $\hat{n}$ is a unit vector which is *perpendicular* to the plane that contains $\vec{a}$ and $\vec{b}$. 
    Note that this cross-product can only be defined in *three-dimensional vector spaces*. The resulting vector 
    $\vec{c}=\vec{a}\times \vec{b} $ will have as components $c_1 = a_2b_3-a_3b_2$, $c_2= a_3b_1 - a_1b_3$, and $c_3= a_1b_2 - a_2b_1$.

### Unit vector and orthonormality

!!! info "Unit vector"
    A special vector is the **unit vector**, which has a norm of 1 *by definition*. A unit vector is often denoted with a hat, rather than an arrow ($\hat{i}$ instead of $\vec{i}$). To find the unit vector in the direction of an arbitrary vector $\vec{v}$, we divide by the norm: $$\hat{v} = \frac{\vec{v}}{|\vec{v}|}$$

!!! info "Orthonormality"    
    Two vectors are said to be **orthonormal** of they are perpendicular (orthogonal) *and* both are unit vectors.

Now we are ready to define in a more formal way what vector spaces are,
an essential concept for the description of quantum mechanics.

### The main properties

The main properties of **vector spaces** are the following:

!!! info ""
    A vector space is **complete upon vector addition**.
    This property means that if two arbitrary vectors  $\vec{a}$ and $\vec{b}$
    are elements of a given vector space ${\mathcal V}^n$,
    then their addition should also be an element of the same vector space 
    $$\vec{a}, \vec{b} \in {\mathcal V}^n, \qquad \vec{c} = (\vec{a} + \vec{b}) \in {\mathcal V}^n  \, ,\qquad \forall\,\, \vec{a}, \vec{b} \,.$$

!!! info "" 
    A vector space is **complete upon scalar multiplication**.
    This property means that when I multiply one arbitrary vector  $\vec{a}$,
    element of the vector space ${\mathcal V}^n$, by a general scalar $\lambda$, the result is another vector which also belongs to the same vector space $$\vec{a} \in {\mathcal V}^n, \qquad \vec{c} = \lambda \vec{a}
    \in {\mathcal V}^n \qquad \forall\,\, \vec{a},\lambda \, .$$
    
The property that a vector space is complete upon scalar multiplication and vector addition is also known as the **closure condition**.

!!! info ""
    There exists a **null element** $\vec{0}$ such that $\vec{a}+\vec{0} =\vec{0}+\vec{a}=\vec{a} $.

!!! info ""
    **Inverse element**: for each vector $\vec{a} \in \mathcal{V}^n$ there exists another
    element of the same vector space, $-\vec{a}$, such that their addition results
    in the null element, $\vec{a} + ( -\vec{a}) = \vec{0}$. This element it called the **inverse element**.

A vector space comes often equipped with various multiplication operations between vectors, such as the scalar product mentioned above
(also known as *inner product*), but also many other operations such as *vector product* or *tensor product*. There are also many other properties, but for what we are interested in right now, these are sufficient.


## 3.3. Matrix representation of vectors

It is advantageous to represent vectors with a notation suitable for matrix manipulation and operations. As we will show in the next lectures, the operations involving states in quantum systems can be expressed in the language of linear algebra.

First of all, let us remind ourselves how we express vectors in the standard Euclidean space. In two dimensions, the position of a point $\vec{r}$ when making explicit the Cartesian basis vectors reads
$$ \vec{r}=x \hat{i}+y\hat{j} \, .$$
As mentioned above, the unit vectors $\hat{i}$ and $\hat{j}$ form an *orthonormal basis* of this vector space, and we call $x$ and $y$ the *components* of $\vec{r}$ with respect to the directions spanned by the basis vectors.

Recall also that the choice of basis vectors is not unique, we can use any other pair of orthonormal unit vectors $\hat{i}$ and $\hat{j}$, and express the vector $\vec{r}$ in terms of these new basis vectors as 
$$ \vec{r}=x'\hat{i}'+y'\hat{j}'=x\hat{i}+y\hat{i} \, ,$$
with $x'\neq x$ and $y'\neq y$. So, while the vector itself does not depend on the basis, the values of its components are basis dependent.

We can also express the vector $\vec{r}$ in the following form
$$ \vec{r} =  \begin{pmatrix}x\\y\end{pmatrix} \, ,$$
which is known as a *column vector*. Note that this notation assumes a specific choice of basis vectors, which is left
implicit and displays only the information on its components along this specific basis.

For instance, if we had chosen another set of basis vectors $\hat{i}'$ and $\hat{j}'$, the components would be $x'$ and $y'$, and the corresponding column vector representing the same vector $\vec{r}$ in such case would be given by
$$ \vec{r}= \begin{pmatrix}x'\\y'\end{pmatrix}.$$

We also know that Euclidean space is equipped with a scalar vector product. 
The scalar product $\vec{r_1}\cdot\vec{r_2}$ of two vectors in 2D Euclidean space is given by
$$ \vec{r_1}\cdot\vec{r_2}=r_1\,r_2\,\cos\theta \, ,$$
where $r_1$ and $r_2$ indicate the *magnitude* (length) of the vectors and $\theta$ indicates its relative angle. Note that the scalar product of two vectors is just a number, and thus it must be *independent of the choice of basis*.

The same scalar product can also be expressed in terms of components of $\vec{r_1}$ and $\vec{r_2}$. When using the $\{ \hat{i}, \hat{j} \}$ basis, the scalar product will be given by
$$ \vec{r_1}\cdot\vec{r_2}=x_1\,x_2\,+\,y_1\,y_2 \, .$$
Note that the same result would be obtained if the basis $\{ \hat{i}', \hat{j}' \}$ 
had been chosen instead 
$$ \vec{r_1}\cdot\vec{r_2}=x_1'\,x_2'\,+\,y_1'\,y_2' \, .$$

The scalar product of two vectors can also be expressed, taking into
account the properties of matrix multiplication, in the following form
$$ \vec{r_1}\cdot\vec{r_2} = \begin{pmatrix}x_1, y_1\end{pmatrix}\begin{pmatrix}x_2\\y_2\end{pmatrix} = x_1x_2+y_1y_2 \, ,$$
where here we say that the vector $\vec{r_1}$ is represented by a *row vector*.

Therefore, we see that the scalar product of vectors in Euclidean space can be expressed as the matrix multiplication of row and column vectors. The same formalism, as we will see in the next class, can be applied for the case of Hilbert spaces in quantum mechanics.

***

## 3.4. Problems

**1)** [:grinning:] Find a unit vector parallel to the sum of $\vec{r}_1$ and $\vec{r}_2$, where we have defined
$$\vec{r}_1=2\vec{i}+4\vec{j}-5\vec{k} \, , $$ and $$\vec{r}_2=\vec{i}+2\vec{j}+3\vec{k} \, .$$.

***

**2)** [:grinning:] If the vectors $\vec{a}$ and $\vec{b}$ may be written in the parametric form
as a function of the parameter $t$ as follows
$$\vec{a}=3t^3\,\vec{i}-2t\,\vec{j}+t^2\,\vec{k}$$ and $$\vec{b}=3\sin{t}\,\vec{i}+2\cos{t}\,\vec{k}$$
Evaluate the following derivatives with respect to the parameter $t$:

**(a)** $ d(\vec{a}\cdot\vec{b}) / dt$.

**(b)** $d \left( \vec{a} \times \vec{b}\right)/dt$.

***

**3)** [:sweat:] Three non-zero vectors $\vec{a}$, $\vec{b}$ and $\vec{c}$ are such that $(\vec{a}+\vec{b})$ is perpendicular to $(\vec{a}+\vec{c})$ and $(\vec{a}-\vec{b})$ is perpendicular to $(\vec{a}-\vec{c})$. Show that $\vec{a}$ is perpendicular to $\vec{b}+\vec{c}$. If the magnitude of the vectors $\vec{a}$, $\vec{b}$ and $\vec{c}$ are in the ratio 1:2:4, find the angle between $\vec{b}$ and $\vec{c}$.

***

**4)** [:grinning:] Find the vector product $\vec{b} \times \vec{c}$ and the triple product $\vec{a}\cdot(\vec{b} \times \vec{c})$, where these three vectors are defined as
$$\vec{a}=\vec{i}+4\vec{j}+\vec{k}\,,$$ and $$\vec{b}=-\vec{i}+2\vec{j}+2\vec{k}\,,$$ and $$\vec{c}=2\vec{i}-\vec{k}\,.$$

***

