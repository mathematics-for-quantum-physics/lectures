# Mathematics for Quantum Physics

*Mathematics for Quantum Mechanics* gives you a compact introduction and review
of the basic mathematical tools commonly used in quantum mechanics. Throughout
the course, we keep quantum mechanics applications in mind, but at the
core, this is still a mathematics course. For this reason, applying what you learned
to examples and exercises is **crucial**!

!!! tip "Learning goals"

    After following this course you will be able to:

    - reproduce elementary formulas from the topics covered.
    - solve mathematical problems encountered in the follow-up courses of the minor.
    - explain Hilbert spaces of (in)finite dimension. 


!!! note "Exercises"
    Each lecture note comes with an extensive set of exercises, and each exercise is labeled according to its difficulty:

    - [:grinning:] easy
    - [:smirk:] intermediate
    - [:sweat:] difficult

With these notes, our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/mathematics-for-quantum-physics/lectures), especially do [let us know](https://gitlab.kwant-project.org/mathematics-for-quantum-physics/lectures/issues/new?issuable_template=typo) if you see a typo!
