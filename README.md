# Mathematics for Quantum Physics
Lecture notes and teaching material used for the Delft University of Technology course TN3105.

The compiled materials are available at https://mathforquantum.quantumtinkerer.tudelft.nl

# Origin and technical support

This repository is based on a template for publishing lecture notes, developed
by Anton Akhmerov, who also hosts such repositories for other courses.

# Version

This a minimal stable version of the website from the branch "enabling search" based on mkdocs-material with a funcitoning website-wide search (without the support for inline jupyter notebook conversion by thebe)

# HOWTOs

## How to add new material to the lecture notes

1. First, create a new merge request. In this way, your edits
   will be pushed to a separate folder, and not directly appear on the website.
   
   Detailed information on how to create a merge request can be found
   [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html), but in most cases these two simple steps are sufficient:
    - create a new branch in the repository on gitlab (either using the gitlab UI, or on the command line and then push to gitlab)
    - on top of the gitlab page you will see a blue "Create merge request" button associated with your new branch. Fill out the information, and don't forget to
start the name of the merge request with "WIP:"

2. Write the new material using [markdown](https://en.wikipedia.org/wiki/Markdown#Example). The markdown files are stored in the `src` folder and have the
ending `.md`. In particular, in markdown you can
    - write math using latex syntax. `$...$` is used for math in the text,
      `$$...$$` for separate equations.
    - highlight certain blocks using the `!!!` syntax. For examples, use
      ```
      !!! check "Example: optional title"
          The text of the example (could have math in it
	        $f(x)$), which must be indented by 4 spaces
      ```
      Other useful blocks are `!!! warning` and `!!! info`

3. Place figures in `docs/figures`

4. If you added a new markdown file that should be linked in the index, you need
   to add it to `mkdocs.yml` under the `nav:` entry.

5. Whenever you push a commit to the branch/merge request, it will automatically be deployed on a preview webpage. This process may take a few minutes. You can find the preview website by going to your merge request. There will be on top a box with the label "Pipeline", and in the box a button "View app". Clicking on "View app" will bring you to the preview webpage.

6. When you are done with the merge request, remove "Draft:" from the title, and notify an instructor.
